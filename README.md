# Requisitos e Instalaciones

En este proyecto encontrarás los pasos a seguir para preparar tu ambiente y equipo.

# Links de descarga

- **Git Bash:** https://git-scm.com/downloads
- **Node Js:** https://nodejs.org/es/download/  https://nodejs.org/download/release/v12.16.3/
- **Angular Cli:** https://www.npmjs.com/package/@angular/cli
- **Typescript:** https://www.npmjs.com/package/typescript
- **Maven:** http://maven.apache.org/download.cgi#Installation
- **Java Oracle:** https://www.oracle.com/java/technologies/javase-jdk11-downloads.html
- **Java OpenJDK**: https://jdk.java.net/archive/
- **Groovy**: https://groovy.apache.org/download.html
- **Groovy Repository:** https://gitlab.com/bg-utp-repository/procesos-en-lote-con-groovy
- **Visual Studio Code:** https://code.visualstudio.com/download
- **MySQL Workbench:** https://dev.mysql.com/downloads/workbench/
- **Intellij Idea Community:** https://www.jetbrains.com/idea/download/
- **Chromedriver:** http://chromedriver.chromium.org/
- **Kafka:** https://www.apache.org/dyn/closer.cgi?path=/kafka/2.7.0/kafka_2.13-2.7.0.tgz
- **Docker** https://docs.docker.com/desktop/

# Links para creación de cuentas

- **Firebase:** https://firebase.google.com/?hl=es 
- **Docker:** https://hub.docker.com/signup
- **IBM Cloud:** https://cloud.ibm.com/login/




